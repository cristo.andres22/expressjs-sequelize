const CORSMiddleware = require('./middleware/cors');
// const SentryMiddleware = require('./middleware/sentry');
const BodyParserMiddlleware = require('./middleware/body-parser');

module.exports = (app) => {
  // SentryMiddleware(app);
  CORSMiddleware(app);
  BodyParserMiddlleware(app);
};
