const models = require('../models');

module.exports = (app, pg) => {
  const model = models(pg);

  app.get('/user', (req, res) => {
    model.userAuth.findAll({}).then((user) => {
      res.json(user);
    }).catch((err) => {
      console.log(err);
      res.sendStatus(400);
    });
  });

  app.get('/user/sale', (req, res) => {
    model.userAuth.findAll({
      include: {
        model: model.sale,
        include: [{
          model: model.saleType,
        }, {
          model: model.debtType,
        }],
      },
      order: [
        ['id', 'ASC'],
      ],
    }).then((user) => {
      res.json(user);
    }).catch((err) => {
      console.log(err);
      res.sendStatus(400);
    });
  });

  app.get('/user/:id/sale', (req, res) => {
    const userId = req.params.id;
    model.userAuth.findOne({
      where: {
        uuid: userId,
      },
      include: {
        model: model.sale,
        include: [{
          model: model.saleType,
        }, {
          model: model.debtType,
        }],
      },
      order: [
        ['id', 'ASC'],
      ],
    }).then((user) => {
      res.json(user);
    }).catch((err) => {
      console.log(err);
      res.sendStatus(400);
    });
  });

  app.delete('/sale/:id', (req, res) => {
    const saleId = req.params.id;
    model.sale.destroy({
      where: {
        id: saleId,
      },
    }).then(() => {
      res.sendStatus(200);
    }).catch((err) => {
      console.log(err);
      res.sendStatus(400);
    });
  });
};
