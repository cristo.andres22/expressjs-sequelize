const pg = require('./datasources').pg;
const index = require('./endpoints/index');
const user = require('./endpoints/user');

module.exports = (app) => {
  index(app);
  user(app, pg);
};
