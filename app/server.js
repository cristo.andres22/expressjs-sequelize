const http = require('http');
const express = require('express');

const middleware = require('./middleware');
const endpoints = require('./endpoints');

const app = express();
const port = process.env.PORT || 5000;
const server = http.createServer(app);

middleware(app);
endpoints(app);

server.listen(port, (err) => {
  if (err) {
    console.error('Unable to listen for connections', err);
    process.exit(1);
  }
  console.log('running on port', port);
});
