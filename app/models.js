const UserAuth = require('./models/user_auth');
const DebtType = require('./models/debt_type');
const Payment = require('./models/payment');
const PositiveBalance = require('./models/positive_balance');
const SalePayment = require('./models/sale_payment');
const SaleType = require('./models/sale_type');
const Sale = require('./models/sale');

const models = (sequelize) => {
  this.sequelize = sequelize;

  this.userAuth = UserAuth(this.sequelize);
  this.debtType = DebtType(this.sequelize);
  this.payment = Payment(this.sequelize);
  this.positiveBalance = PositiveBalance(this.sequelize);
  this.salePayment = SalePayment(this.sequelize);
  this.saleType = SaleType(this.sequelize);
  this.sale = Sale(this.sequelize);

// Relations

  this.userAuth.hasMany(this.positiveBalance);
  this.positiveBalance.belongsTo(this.userAuth);

  this.userAuth.hasMany(this.sale);
  this.sale.belongsTo(this.userAuth);

  this.debtType.hasMany(this.sale);
  this.sale.belongsTo(this.debtType);

  this.saleType.hasMany(this.sale);
  this.sale.belongsTo(this.saleType);

  this.sale.belongsToMany(this.payment, { through: this.salePayment });
  this.payment.belongsToMany(this.sale, { through: this.salePayment });

  return this;
};

module.exports = models;
module.exports.models = models;
