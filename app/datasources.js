const Sequelize = require('sequelize');

module.exports = {
  pg: new Sequelize(process.env.PG_DSN, {
    dialect: 'postgres',
    dialectOptions: {
      ssl: true,
    },
  }),
};
