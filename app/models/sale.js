const DataTypes = require('sequelize').DataTypes;

module.exports = (sequelize) => {
  const Sale = sequelize.define('sale', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    created_at: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    updated_at: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    deleted_at: {
      type: DataTypes.TIME,
      allowNull: true
    },
    cant: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    user_auth_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'user_auth',
        key: 'id'
      }
    },
    debt_type_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'debt_type',
        key: 'id'
      }
    },
    sale_type_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'sale_type',
        key: 'id'
      }
    },
    amount: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    balance: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    pay: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    tableName: 'sale',
    paranoid: true,
    underscored: true,
  });

  return Sale;
};
