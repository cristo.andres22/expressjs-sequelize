const DataTypes = require('sequelize').DataTypes;

module.exports = (sequelize) => {
  const PositiveBalance = sequelize.define('positive_balance', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    created_at: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    updated_at: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    deleted_at: {
      type: DataTypes.TIME,
      allowNull: true
    },
    amount: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    user_auth_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'user_auth',
        key: 'id'
      }
    }
  }, {
    tableName: 'positive_balance',
    paranoid: true,
    underscored: true,
  });

  return PositiveBalance;
};
