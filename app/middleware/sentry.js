const Raven = require('raven');

module.exports = (app) => {
  // Must configure Raven before doing anything else with it
  Raven.config(process.env.SENTRY_DSN).install();
  // The request handler must be the first middleware on the app
  app.use(Raven.requestHandler());

  app.get('/', () => {
    throw new Error('Broke!');
  });

// The error handler must be before any other error middleware
  app.use(Raven.errorHandler());

// Optional fallthrough error handler
  app.use((err, req, res, next) => {
    // The error id is attached to `res.sentry` to be returned
    // and optionally displayed to the user for support.
    res.statusCode = 500;
    // res.end(res.sentry);
    next();
  });
};
