# Kastor accounting
[![build status](https://gitlab.com/k-3/kastor-accounting/badges/master/build.svg)](https://gitlab.com/k-3/kastor-accounting/commits/master)
[![coverage report](https://gitlab.com/k-3/kastor-accounting/badges/master/coverage.svg)](https://gitlab.com/k-3/kastor-accounting/commits/master)

*Accounting API of the Kastor management system.*

For the endpoint details go to the wiki: [Kastor accounting wiki](https://gitlab.com/k-3/kastor-accounting/wikis/home)

